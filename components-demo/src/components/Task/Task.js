import React from 'react';
import './Task.css';


const Task = (props) =>{
  return (
    props.task.map((task, id) => {
      return(
        <div key={id} className='Border'>
          <div className='Bg'>
            <p className='Text'>{task.text}</p>
            <button className='Btn' onClick={() => props.onClick(task.id)}>X</button>
          </div>

        </div>
      )
    })
  )
};

export default Task;