import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = (props) => {
  return(
    <div className='FormAdd'>
      <input className='Val' onChange={props.change} value={props.value} type="text"/>
      <button className='Button' onClick={props.onClick}>Add</button>
    </div>
  )
};

export default AddTaskForm;