import React, { Component, Fragment } from 'react';
import AddTaskForm from './components/AddTaskForm/AddTaskForm';
import Task from './components/Task/Task';
import './App.css';

class App extends Component {
  state = {
    tasks: [],
    taskText: '',
    currentTask: 1
  };
  changeHendler = (event) =>{
    this.setState({
      taskText: event.target.value
    })
  };
  addNewTask = () =>{
    const tasks = [...this.state.tasks];
    const newTask = {text: this.state.taskText, id: this.state.currentTask + 1};

    tasks.push(newTask);

    this.setState({tasks: tasks, currentTask: this.state.currentTask + 1, taskText: ''})

  };
  deleteTask = (id) => {
    const tasks = [...this.state.tasks];
    const index = tasks.findIndex((task) => {
      return(
        task.id === id
      )
    });
    tasks.splice(index, 1);
    this.setState({tasks: tasks, currentTask: this.state.currentTask - 1})
  };
  render() {
    return (
      <Fragment>
        <AddTaskForm value={this.state.taskText} change={(event) => this.changeHendler(event)} onClick={() => this.addNewTask()}/>
        <Task task={this.state.tasks} onClick={(id) => this.deleteTask(id)}/>
      </Fragment>
    );
  }
}

export default App;
